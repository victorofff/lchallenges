package com.luxoft.challenges.c21;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Util {

    public static void makeJump(ChessBoard localBoard, ChessBoard.Move move) {
        localBoard.getBoard()[move.getP1()] = 0;
        localBoard.getBoard()[move.getP2()] = 0;
        localBoard.getBoard()[move.getP3()] = 1;
    }

    public static void revertJump(ChessBoard localBoard, ChessBoard.Move move) {
        localBoard.getBoard()[move.getP1()] = 1;
        localBoard.getBoard()[move.getP2()] = 1;
        localBoard.getBoard()[move.getP3()] = 0;
    }

    public static void addToChangeStack(ChessBoard localBoard, ChessBoard.Move move) {
        localBoard.getBoardChangeStack()[move.getP1()].push(move.getMoveId());
        localBoard.getBoardChangeStack()[move.getP2()].push(move.getMoveId());
        localBoard.getBoardChangeStack()[move.getP3()].push(move.getMoveId());
    }

    public static void removeFromChangeStack(ChessBoard localBoard, ChessBoard.Move move) {
        localBoard.getBoardChangeStack()[move.getP1()].pop();
        localBoard.getBoardChangeStack()[move.getP2()].pop();
        localBoard.getBoardChangeStack()[move.getP3()].pop();
    }

        public static int lastPmove(ChessBoard localBoard, ChessBoard.Move move) {
        Integer p1Last = localBoard.getBoardChangeStack()[move.getP1()].peekLast();
        Integer p2Last = localBoard.getBoardChangeStack()[move.getP2()].peekLast();
        Integer p3Last = localBoard.getBoardChangeStack()[move.getP3()].peekLast();

        return Math.max(Math.max(p1Last != null ? p1Last : -1, p2Last != null ? p2Last : -1), p3Last != null ? p3Last : -1);
    }

    public static int getIndexFromRowColumn(int y, int x) {
        int startIdx = y * ChessBoard.ROW_COLUMN_SIZE;
        return startIdx + x / 2;
    }

    public static int[] getRowColumnFromIndex(int idx) {
        int row = idx / ChessBoard.ROW_COLUMN_SIZE;
        int column = (idx - row * ChessBoard.ROW_COLUMN_SIZE) * 2;
        if (row % 2 != 0) {
            column += 1;
        }

        return new int[]{row, column};
    }

    public static double manhattanDistance(ChessBoard localBoard, int numPegs) {
        int man = 0;
        for (int i = 0; i < ChessBoard.CHESS_SIZE; i++) {
            for (int j = 0; j < ChessBoard.CHESS_SIZE; j++) {
                int idx = Util.getIndexFromRowColumn(i, j);
                if (localBoard.getBoard()[idx] == 1) {

                    for (int i1 = 0; i1 < ChessBoard.CHESS_SIZE; i1++) {
                        for (int j1 = 0; j1 < ChessBoard.CHESS_SIZE; j1++) {
                            int idx1 = Util.getIndexFromRowColumn(i1, j1);
                            if (localBoard.getBoard()[idx1] == 1) {
                                man += Math.abs(i - i1) + Math.abs(j - j1);
                            }
                        }
                    }

                }
            }
        }
        return (double) man / (2 * numPegs);
    }
}
