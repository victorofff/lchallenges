package com.luxoft.challenges.c21;

import com.luxoft.challenges.c21.ChessBoard.Move;
import java.util.List;

public class BoardEnginePseudoParallelDecorator implements BoardEngine {

    private final int emptyY;

    private final int emptyX;

    private final BoardEngine boardEngine;

    public BoardEnginePseudoParallelDecorator(int emptyY, int emptyX) {

        this.emptyY = emptyY;
        this.emptyX = emptyX;

        boardEngine = new BoardEngineImpl(emptyY, emptyX);

    }


    @Override
    public Result runStrategy(BoardStrategy boardStrategy) {

        long started = System.currentTimeMillis();
        if (boardStrategy instanceof BoardStrategyFirstProcessAroundEmpty boardStrategyImpl) {

            boolean result = false;
            int iterations = 0;
            long totalTimeSec = 0;

            BoardEngineImpl initialBoard = (BoardEngineImpl) boardEngine;
            List<Move> allMoves = initialBoard.getChessBoard().getMoveListFirstEmpty();
            List<Move> firstMoves = allMoves.stream().filter(Move::isInitial).toList();

            BoardEngineImpl contextEngine = null;
            for (Move firstMove : firstMoves) {

                contextEngine = new BoardEngineImpl(emptyY, emptyX);
                long moveStarted = System.currentTimeMillis();
                boolean moveResult = boardStrategyImpl.accept(contextEngine, firstMove);
                long moveTimeSec = (System.currentTimeMillis() - moveStarted) / 1000;
                totalTimeSec += moveTimeSec;

                System.out.println("An attempt took " + moveTimeSec + " sec and the result " + moveResult + ", iterations " + contextEngine.getIterations());
                iterations += contextEngine.getIterations();

                if (moveResult) {
                    result = true;
                    break;
                }
            }

            if (result) {
                System.out.printf("Totally took %d seconds, number of iterations %d %n", totalTimeSec, iterations);
                contextEngine.printBoardChanges();
            }
            return new Result((System.currentTimeMillis() - started) / 1000, result, iterations);

        } else {
            throw new IllegalArgumentException("Parallel execution is not supported");
        }


    }


}
