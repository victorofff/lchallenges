package com.luxoft.challenges.c21;

import gnu.trove.TIntHashSet;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

public class BoardEngineImpl implements BoardEngine {

    @Getter
    private final List<Integer> steps = new ArrayList<>();

    @Getter
    private final ChessBoard chessBoard = new ChessBoard();

    private final int emptyY;

    private final int emptyX;

    @Getter
    private final TIntHashSet visited = new TIntHashSet();

    private boolean finished;

    @Getter
    private int remainingPegs = ChessBoard.NUM_PEGS;

    @Getter
    private int iterations;

    @Getter
    private boolean result;

    @Getter
    private long timeSec = -1;

    public BoardEngineImpl(int emptyY, int emptyX) {

        this.emptyY = emptyY;
        this.emptyX = emptyX;

        chessBoard.init(emptyY, emptyX);
    }

    public void incrementIterations() {
        iterations++;
    }

    public void incrementPegs() {
        remainingPegs++;
    }

    public void decrementPegs() {
        remainingPegs--;
    }

    public void addStep(ChessBoard.Move move) {
        steps.add(move.getMoveId());
    }

    public int removeStep() {
        return steps.remove(steps.size() - 1);
    }


    public Result runStrategy(BoardStrategy boardStrategy) {

        if (finished) {
            throw new IllegalArgumentException("Invalid state");
        }

        long started = System.currentTimeMillis();
        result = boardStrategy.accept(this);
        finished = true;
        if (result) {
            System.out.println("Ok");
        } else {
            System.out.println("Failed");
        }

        timeSec = (System.currentTimeMillis() - started ) / 1000;
        System.out.printf("Took %d seconds, number of iterations %d %n", timeSec, iterations);
        printBoardChanges();
        return new Result(timeSec, result, iterations);

    }


    public boolean checkFinished() {
        if (remainingPegs <= 1) {
            return true;
        }

        iterations++;
        return false;
    }

    public void printBoardChanges() {
        ChessBoard localBoard = new ChessBoard();
        localBoard.init(emptyY, emptyX);
        System.out.println("-------");
        System.out.println(localBoard.boardToString());
        for (int i = 0; i < steps.size(); i++) {
            int moveId = steps.get(i);
            ChessBoard.Move move = chessBoard.getMoveMap().get(moveId);

            Util.makeJump(localBoard, move);

            printStep(i, move);
            System.out.println("-------");
            System.out.println(localBoard.boardToString());
        }
    }

    public void printBoard() {
        System.out.println("-------");
        System.out.println(chessBoard.boardToString());
    }


    private void printStep(int i, ChessBoard.Move move) {
        int[] source = Util.getRowColumnFromIndex(move.getP1());
        int[] middle = Util.getRowColumnFromIndex(move.getP2());
        int[] destination = Util.getRowColumnFromIndex(move.getP3());

        System.out.printf("Move # %d From %d%d over %d%d to %d%d %n", i, source[0], source[1], middle[0], middle[1], destination[0], destination[1]);
    }

}
