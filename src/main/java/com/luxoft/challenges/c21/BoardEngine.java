package com.luxoft.challenges.c21;

public interface BoardEngine {
    Result runStrategy(BoardStrategy boardStrategy);
}
