package com.luxoft.challenges.c21;

public interface BoardStrategy {
    boolean accept(BoardEngine boardEngine);
}
