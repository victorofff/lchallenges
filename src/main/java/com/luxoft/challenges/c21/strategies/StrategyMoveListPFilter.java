package com.luxoft.challenges.c21.strategies;

import com.luxoft.challenges.c21.BoardEngine;
import com.luxoft.challenges.c21.BoardEngineImpl;
import com.luxoft.challenges.c21.BoardStrategy;
import com.luxoft.challenges.c21.ChessBoard;
import com.luxoft.challenges.c21.ChessBoard.Move;
import com.luxoft.challenges.c21.Util;
import java.util.List;

public class StrategyMoveListPFilter implements BoardStrategy  {
    @Override
    public boolean accept(BoardEngine boardEngine) {

        BoardEngineImpl boardEngineImpl = (BoardEngineImpl)boardEngine;

        List<Move> moveList = boardEngineImpl.getChessBoard().getMoveList();
        ChessBoard chessBoard = boardEngineImpl.getChessBoard();

        int xMove = 0; // index in move list
        int moveNr = 0; // index of step
        while (true) {


            Move move = moveList.get(xMove);

            //int id = boardEngineImpl.getChessBoard().getBoardId();
            //boolean continueThatCheck = !boardEngineImpl.getVisited().contains(id);
            boolean goNextMove = !(chessBoard.getBoard()[move.getP1()] == 1 && chessBoard.getBoard()[move.getP2()] == 1 && chessBoard.getBoard()[move.getP3()] == 0);

//            if (!goNextMove) {
//                int pm = Util.lastPmove(chessBoard, move) + 1;
//                for(int i = pm; i < moveNr; i++) {
//                    if (i > xMove) {
//                        goNextMove = true;
//                        break;
//                    }
//                }
//             }

            if (!goNextMove) {

                // move
                Util.makeJump(chessBoard, move);
                boardEngineImpl.addStep(move);
                boardEngineImpl.decrementPegs();
                moveNr++;



                // ok
                if (boardEngineImpl.getRemainingPegs() == 1) {
                    return true;
                }

                xMove = 0;

            } else {
                xMove++;

                boolean cached = false;
                while (xMove >= moveList.size() || cached) {

                    if (boardEngineImpl.getSteps().isEmpty()) {
                        return false;
                    }

                    //boardEngineImpl.printBoard();

                    boardEngineImpl.getVisited().add(boardEngineImpl.getChessBoard().getBoardId());

                    xMove = boardEngineImpl.removeStep();
                    Util.revertJump(chessBoard, moveList.get(xMove));
                    xMove++;
                    boardEngineImpl.incrementPegs();
                    moveNr--;

                    // we returned back, check id again
                    int id = boardEngineImpl.getChessBoard().getBoardId();
                    cached = boardEngineImpl.getVisited().contains(id);

                }

            }
        }

    }

    private static boolean checkPFilter(BoardEngineImpl boardEngineImpl, Move move) {
        int pm = Util.lastPmove(boardEngineImpl.getChessBoard(), move) + 1;
        for (int k = pm; k < move.getMoveId(); k++) {
            if ( k > boardEngineImpl.getSteps().size()) {
                return true;
            }
        }
        return false;
    }

}
