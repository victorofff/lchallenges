package com.luxoft.challenges.c21.strategies;

import com.luxoft.challenges.c21.BoardEngine;
import com.luxoft.challenges.c21.BoardEngineImpl;
import com.luxoft.challenges.c21.BoardStrategy;
import com.luxoft.challenges.c21.ChessBoard;
import com.luxoft.challenges.c21.ChessBoard.Move;
import com.luxoft.challenges.c21.Util;

public class StrategyBoardOneTwo implements BoardStrategy {

    @Override
    public boolean accept(BoardEngine boardEngine) {
        BoardEngineImpl boardEngineImpl = (BoardEngineImpl)boardEngine;

        if (boardEngineImpl.checkFinished()) {
            return true;
        }

        int id = boardEngineImpl.getChessBoard().getBoardId();
        if (boardEngineImpl.getVisited().contains(id)) {
            return false;
        }

        Move[][] moveFastP1 = boardEngineImpl.getChessBoard().getMoveFastP1();
        Move[][] moveFastP3 = boardEngineImpl.getChessBoard().getMoveFastP3();

        for (int i = 0; i < ChessBoard.BOARD_SIZE; i++) {

            if (boardEngineImpl.getRemainingPegs() > ChessBoard.NUM_PEGS_HALF) {

                if (boardEngineImpl.getChessBoard().getBoard()[i] == 0) {
                    for (Move move : moveFastP3[i]) {
                        if (boardEngineImpl.getChessBoard().getBoard()[move.getP1()] == 1 && boardEngineImpl.getChessBoard().getBoard()[move.getP2()] == 1) {
                            Util.makeJump(boardEngineImpl.getChessBoard(), move);
                            boardEngineImpl.decrementPegs();

                            if (accept(boardEngine)) {
                                boardEngineImpl.addStep(move);
                                return true;
                            } else {
                                Util.revertJump(boardEngineImpl.getChessBoard(), move);
                                boardEngineImpl.incrementPegs();
                            }
                        }
                    }
                }
            } else {

                if (boardEngineImpl.getChessBoard().getBoard()[i] == 1) {
                    for (Move move : moveFastP1[i]) {
                        if (boardEngineImpl.getChessBoard().getBoard()[move.getP2()] == 1 && boardEngineImpl.getChessBoard().getBoard()[move.getP3()] == 0) {
                            Util.makeJump(boardEngineImpl.getChessBoard(), move);
                            boardEngineImpl.decrementPegs();

                            if (accept(boardEngine)) {
                                boardEngineImpl.addStep(move);
                                return true;
                            } else {
                                Util.revertJump(boardEngineImpl.getChessBoard(), move);
                                boardEngineImpl.incrementPegs();
                            }
                        }
                    }
                }
            }

        }

        boardEngineImpl.getVisited().add(id);
        return false;
    }
}
