package com.luxoft.challenges.c21.strategies;

import com.luxoft.challenges.c21.BoardStrategyFirstProcessAroundEmpty;
import com.luxoft.challenges.c21.BoardEngine;
import com.luxoft.challenges.c21.BoardEngineImpl;
import com.luxoft.challenges.c21.BoardStrategy;
import com.luxoft.challenges.c21.ChessBoard.Move;
import com.luxoft.challenges.c21.Util;
import java.util.Collections;
import java.util.List;

public class StrategyMoveListFirstEmpty implements BoardStrategy, BoardStrategyFirstProcessAroundEmpty {
    @Override
    public boolean accept(BoardEngine boardEngine) {

        BoardEngineImpl boardEngineImpl = (BoardEngineImpl)boardEngine;
        if (boardEngineImpl.checkFinished()) {
            return true;
        }

        int id = boardEngineImpl.getChessBoard().getBoardId();
        if (boardEngineImpl.getVisited().contains(id)) {
            return false;
        }

        List<Move> moveList = boardEngineImpl.getChessBoard().getMoveListFirstEmpty();
        for (Move move : moveList) {
            if (boardEngineImpl.getChessBoard().getBoard()[move.getP1()] == 1 && boardEngineImpl.getChessBoard().getBoard()[move.getP2()] == 1 && boardEngineImpl.getChessBoard().getBoard()[move.getP3()] == 0) {
                Util.makeJump(boardEngineImpl.getChessBoard(), move);
                boardEngineImpl.decrementPegs();

                if (accept(boardEngine)) {
                    boardEngineImpl.addStep(move);
                    return true;
                } else {
                    Util.revertJump(boardEngineImpl.getChessBoard(), move);
                    boardEngineImpl.incrementPegs();
                }
            }

        }

        boardEngineImpl.getVisited().add(id);
        return false;

    }

    @Override
    public boolean accept(BoardEngine boardEngine, Move firstMove) {
        BoardEngineImpl boardEngineImpl = (BoardEngineImpl)boardEngine;
        if (boardEngineImpl.checkFinished()) {
            return true;
        }

        int id = boardEngineImpl.getChessBoard().getBoardId();
        if (boardEngineImpl.getVisited().contains(id)) {
            return false;
        }

        List<Move> movesList = firstMove != null ? Collections.singletonList(firstMove) : boardEngineImpl.getChessBoard().getMoveListFirstEmpty();


        for (Move move : movesList) {
            if (boardEngineImpl.getChessBoard().getBoard()[move.getP1()] == 1 && boardEngineImpl.getChessBoard().getBoard()[move.getP2()] == 1 && boardEngineImpl.getChessBoard().getBoard()[move.getP3()] == 0) {
                Util.makeJump(boardEngineImpl.getChessBoard(), move);
                boardEngineImpl.decrementPegs();

                if (accept(boardEngine, null)) {
                    boardEngineImpl.addStep(move);
                    return true;
                } else {
                    Util.revertJump(boardEngineImpl.getChessBoard(), move);
                    boardEngineImpl.incrementPegs();
                }
            }

        }

        boardEngineImpl.getVisited().add(id);
        return false;
    }
}
