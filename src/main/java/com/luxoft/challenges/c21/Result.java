package com.luxoft.challenges.c21;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Result {

    private final long timeSec;
    private final boolean isOk;
    private final int iterations;
}
