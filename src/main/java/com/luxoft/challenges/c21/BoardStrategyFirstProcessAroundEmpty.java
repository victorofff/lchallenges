package com.luxoft.challenges.c21;

import com.luxoft.challenges.c21.ChessBoard.Move;

public interface BoardStrategyFirstProcessAroundEmpty {
    boolean accept(BoardEngine boardEngine, Move firstMove);
}
