package com.luxoft.challenges.c21;

import com.luxoft.challenges.c21.strategies.StrategyMoveListFirstEmpty;
import com.luxoft.challenges.c21.strategies.StrategyMoveListPFilter;

public class Main {
    public static void main(String[] args) {
        //BoardEngine boardEngine = new BoardEngineImpl(5, 1);
        //BoardEngine boardEngine = new BoardEngineImpl(5, 3);
        BoardEngineImpl boardEngine = new BoardEngineImpl(3, 3);
        //BoardEngine boardEngine = new BoardEngineImpl(0, 4);
        //boardEngine.run(boardEngine.getRunStrategyMoveArrayP1OrderModified());
        //boardEngine.runStrategy(new StrategyBoardOneTwo());

        //BoardEngine boardEngine = new BoardEngineImpl(4, 4);
        //boardEngine.runStrategy(new StrategyMoveListFirstEmpty());
        boardEngine.runStrategy(new StrategyMoveListPFilter());
    }
}
