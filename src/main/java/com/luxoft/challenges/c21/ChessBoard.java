package com.luxoft.challenges.c21;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

public class ChessBoard {

    @Getter
    @RequiredArgsConstructor
    public static class Move {

        private final int moveId;
        private final int p1;
        private final int p2;
        private final int p3;
        @Setter
        private boolean initial;
    }

    public static final int CHESS_SIZE = 8;
    public static final int ROW_COLUMN_SIZE = 4;
    public static final int BOARD_SIZE = 32;
    public static final int NUM_PEGS = 31;
    public static final int NUM_PEGS_HALF = NUM_PEGS / 2;
    public static final int MAX_STEPS = 30;

    @Getter
    private final Map<Integer, Move> moveMap = new TreeMap<>();

    @Getter
    private List<Move> moveList = new ArrayList<>();

    @Getter
    private List<Move> moveListFirstEmpty = new ArrayList<>();

    @Getter
    private final Move[][] moveFastP1 = new Move[BOARD_SIZE][];

    @Getter
    private final Move[][] moveFastP3 = new Move[BOARD_SIZE][];

    @Getter
    private final Move[][] moveFastFirstEmpty = new Move[BOARD_SIZE][];

    private static final byte[] MOVE_DIRS = new byte[]{
        0b0001, 0b1001, 0b1001, 0b1000, // 0-3
        0b0001, 0b1001, 0b1001, 0b1000, // 4 -7
        0b0011, 0b1111, 0b1111, 0b1100, // 8 - 11
        0b0011, 0b1111, 0b1111, 0b1100, // 12 - 15
        0b0011, 0b1111, 0b1111, 0b1100, // 16 - 19
        0b0011, 0b1111, 0b1111, 0b1100, // 20 - 23
        0b0010, 0b0110, 0b0110, 0b0100, // 24 - 27
        0b0010, 0b0110, 0b0110, 0b0100 // 28 - 31
    };

    private static final int[] OFFSET_ODDS = {4, -4, -3, 5};
    private static final int[] OFFSET_EVENS = {3, -5, -4, 4};
    private static final int[] FIRST_STEP = {7, -9, -7, 9};

    @Getter
    private final byte[] board = new byte[BOARD_SIZE];

    @Getter
    private final Deque<Integer>[] boardChangeStack =
        IntStream.range(0, BOARD_SIZE)
            .mapToObj(element -> new ArrayDeque<>()).toList().toArray(new ArrayDeque[0]);

    public ChessBoard() {
        createMoveList();
    }

    public void init(int emptyY, int emptyX) {
        prepareMoveList();
        populateBoardInitial(emptyY, emptyX);
        prepareFastMoveArrayP1();
        prepareFastMoveArrayP3();
        prepareFastMoveArrayFirstAroundEmpty(emptyY, emptyX);
    }


    public List<Move> getMoveList() {
        return new ArrayList<>(moveMap.values());
    }


    public void setBoardValueAt(int y, int x, byte value) {
        int idx = Util.getIndexFromRowColumn(y, x);
        int[] rc = Util.getRowColumnFromIndex(idx);
        if (rc[0] != y || rc[1] != x) {
            throw new IllegalArgumentException("Empty cell coordinates cannot point to the wall");
        }

        board[idx] = value;
    }


    public String boardToString() {
        return printBoardHelper(board);
    }



    public int getBoardId() {
        int[] symmetry = {0, 0};
        int mask = 1;
        for (int i = 0; i < BOARD_SIZE; i++) {
            int o = BOARD_SIZE - 1 - i;
            if (board[i] == 1) {
                symmetry[0] |= mask;
            }
            if (board[o] == 1) {
                symmetry[1] |= mask;
            }
            mask <<= 1;
        }

        return Math.min(symmetry[0], symmetry[1]);
    }

    /*package*/
    void prepareFastMoveArrayP1() {

        Map<Integer, List<Move>> moveListSorted = moveMap.values().stream()
            .collect(Collectors.groupingBy(
                Move::getP1, TreeMap::new, Collectors.toList()));

        moveListSorted.forEach((key, value) -> {
            moveFastP1[key] = value.toArray(new Move[0]);
        });

    }


    /*package*/
    void prepareFastMoveArrayP3() {

        Map<Integer, List<Move>> moveListSorted = moveMap.values().stream()
            .collect(Collectors.groupingBy(
                Move::getP3, TreeMap::new, Collectors.toList()));

        moveListSorted.forEach((key, value) -> {
            moveFastP3[key] = value.toArray(new Move[0]);
        });

    }

    private void prepareMoveList() {
        moveList = new ArrayList<>(moveMap.values());
    }

    private void prepareFastMoveArrayFirstAroundEmpty(int y, int x) {

        moveListFirstEmpty = moveFirstStepsToTop(y, x);

        Map<Integer, List<Move>> moveListSorted = moveListFirstEmpty.stream()
            .collect(Collectors.groupingBy(
                Move::getP1, LinkedHashMap::new, Collectors.toList()));

        int idx = 0;
        for (Map.Entry<Integer, List<Move>> entry : moveListSorted.entrySet()) {
            moveFastFirstEmpty[idx++] = entry.getValue().toArray(new Move[0]);
        }

    }


    /*package*/
    void populateBoardInitial(int emptyY, int emptyX) {
        Arrays.fill(board, (byte) 1);
        setBoardValueAt(emptyY, emptyX, (byte) 0);
    }

    /*package*/
    List<Move> moveFirstStepsToTop(int y, int x) {
        List<Move> localMoveList = new ArrayList<>(moveMap.values());

        int sourceIdx = Util.getIndexFromRowColumn(y, x);
        List<Integer> surroundPoints = new ArrayList<>();
        for (int o = 0; o < FIRST_STEP.length; o++) {
            int offsetIdx = FIRST_STEP.length - o - 1;
            int moveAllowed = MOVE_DIRS[sourceIdx] & (1 << offsetIdx);

            if (moveAllowed > 0) {
                surroundPoints.add(sourceIdx + FIRST_STEP[o]);
            }
        }

        List<Move> toInsert = new ArrayList<>();
        for (int surroundPoint : surroundPoints) {
            for (Iterator<Move> it = localMoveList.iterator(); it.hasNext(); ) {
                Move nextMove = it.next();
                if (nextMove.p1 == surroundPoint) {
                    toInsert.add(nextMove);
                    nextMove.setInitial(true);
                    it.remove();
                }
            }
        }

        localMoveList.addAll(0, toInsert);
        return localMoveList;
    }


    private String printBoardHelper(byte[] board) {
        StringBuilder result = new StringBuilder();
        for (int y = 0; y < CHESS_SIZE; y++) {
            boolean evenRow = y % 2 == 0;
            int startIdx = y % 2 == 0 ? 0 : 1;

            StringBuilder row = new StringBuilder();

            if (y == 0) {
                result.append("[ ]");
            }
            for (int x = startIdx; x < CHESS_SIZE; x += 2) {
                int idx = Util.getIndexFromRowColumn(y, x);
                if (y == 0) {
                    result.append("[").append(x).append("]").append("[").append(x + 1).append("]");
                }

                row.append(evenRow ? board[idx] == 0 ? "[ ][.]" : "[X][.]" : board[idx] == 0 ? "[.][ ]" : "[.][X]");
            }

            result.append(y == 0 ? "\n" : "").append("[").append(y).append("]").append(row).append("\n");
        }
        return result.toString();
    }


    private void createMoveList() {
        int[] offsetTable1;
        int[] offsetTable2;
        int idx = 0;
        for (int i = 0; i < BOARD_SIZE; i++) {
            int[] rc = Util.getRowColumnFromIndex(i);
            int row = rc[0];

            if (row % 2 == 0) {
                offsetTable1 = OFFSET_EVENS;
                offsetTable2 = OFFSET_ODDS;
            } else {
                offsetTable1 = OFFSET_ODDS;
                offsetTable2 = OFFSET_EVENS;
            }

            int[] offsetTable = row % 2 == 0 ? OFFSET_EVENS : OFFSET_ODDS;
            for (int o = 0; o < offsetTable.length; o++) {
                int offsetIdx = offsetTable.length - o - 1;
                int moveAllowed = MOVE_DIRS[i] & (1 << offsetIdx);

                if (moveAllowed > 0) {
                    int p2 = i + offsetTable1[o];
                    int p3 = p2 + offsetTable2[o];

                    Move move = new Move(idx++, i, p2, p3);
                    moveMap.put(move.getMoveId(), move);
                }
            }

        }
    }


}
