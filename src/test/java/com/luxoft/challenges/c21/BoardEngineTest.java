package com.luxoft.challenges.c21;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;

import com.luxoft.challenges.c21.strategies.StrategyBoardOne;
import com.luxoft.challenges.c21.strategies.StrategyBoardOneTwo;
import com.luxoft.challenges.c21.strategies.StrategyBoardZero;
import com.luxoft.challenges.c21.strategies.StrategyMoveArrayP1Order;
import com.luxoft.challenges.c21.strategies.StrategyMoveArrayP1OrderModified;
import com.luxoft.challenges.c21.strategies.StrategyMoveListFirstEmpty;
import com.luxoft.challenges.c21.strategies.StrategyMoveListPFilter;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class BoardEngineTest {

    private static final long TIMEOUT = 80L;


    @ParameterizedTest
    @MethodSource("rangeProvider")
    void testBoardEngineAllStrategyBoardOneTwo(int idx) {
        int[] rc = Util.getRowColumnFromIndex(idx);

        BoardEngine boardEngine = new BoardEngineImpl(rc[0], rc[1]);
        await().atMost(TIMEOUT, SECONDS).until(() -> {
            Result result = boardEngine.runStrategy(new StrategyBoardOneTwo());
            return result.isOk();
        });

    }

    @ParameterizedTest
    @MethodSource("rangeProvider")
    void testBoardEngineAllStrategyBoardOne(int idx) {
        int[] rc = Util.getRowColumnFromIndex(idx);

        BoardEngine boardEngine = new BoardEngineImpl(rc[0], rc[1]);
        await().atMost(TIMEOUT, SECONDS).until(() -> {
            Result result = boardEngine.runStrategy(new StrategyBoardOne());
            return result.isOk();
        });

    }

    @ParameterizedTest
    @MethodSource("rangeProvider")
    void testBoardEngineAllStrategyBoardZero(int idx) {
        int[] rc = Util.getRowColumnFromIndex(idx);

        BoardEngine boardEngine = new BoardEngineImpl(rc[0], rc[1]);
        await().atMost(TIMEOUT, SECONDS).until(() -> {
            Result result = boardEngine.runStrategy(new StrategyBoardZero());
            return result.isOk();
        });

    }

    @ParameterizedTest
    @MethodSource("rangeProvider")
    void testBoardEngineAllStrategyMoveArrayP1Order(int idx) {
        int[] rc = Util.getRowColumnFromIndex(idx);

        BoardEngine boardEngine = new BoardEngineImpl(rc[0], rc[1]);
        await().atMost(TIMEOUT, SECONDS).until(() -> {
            Result result = boardEngine.runStrategy(new StrategyMoveArrayP1Order());
            return result.isOk();
        });

    }

    @ParameterizedTest
    @MethodSource("rangeProvider")
    void testBoardEngineAllStrategyMoveArrayP1OrderModified(int idx) {
        int[] rc = Util.getRowColumnFromIndex(idx);

        BoardEngine boardEngine = new BoardEngineImpl(rc[0], rc[1]);
        await().atMost(TIMEOUT, SECONDS).until(() -> {
            Result result = boardEngine.runStrategy(new StrategyMoveArrayP1OrderModified());
            return result.isOk();
        });

    }

    @ParameterizedTest
    @MethodSource("rangeProvider")
    void testBoardEngineAllStrategyMoveListFirstEmpty(int idx) {
        int[] rc = Util.getRowColumnFromIndex(idx);

        BoardEngine boardEngine = new BoardEngineImpl(rc[0], rc[1]);
        await().atMost(TIMEOUT, SECONDS).until(() -> {
            Result result = boardEngine.runStrategy(new StrategyMoveListFirstEmpty());
            return result.isOk();
        });

    }

    @ParameterizedTest
    @MethodSource("rangeProvider")
    void testBoardEngineParallelDecorator(int idx) {
        int[] rc = Util.getRowColumnFromIndex(idx);

        BoardEngine boardEngine = new BoardEnginePseudoParallelDecorator(rc[0], rc[1]);
        await().atMost(TIMEOUT, SECONDS).until(() -> {
            Result result = boardEngine.runStrategy(new StrategyMoveListFirstEmpty());
            return result.isOk();
        });

    }

    @ParameterizedTest
    @MethodSource("rangeProvider")
    void testBoardEngineStrategyMoveListPFilter(int idx) {
        int[] rc = Util.getRowColumnFromIndex(idx);

        BoardEngine boardEngine = new BoardEngineImpl(rc[0], rc[1]);
        await().atMost(TIMEOUT, SECONDS).until(() -> {
            Result result = boardEngine.runStrategy(new StrategyMoveListPFilter());
            return result.isOk();
        });

    }


    private static Stream<Integer> rangeProvider() {
        return IntStream.range(0, ChessBoard.BOARD_SIZE).boxed();
    }


}