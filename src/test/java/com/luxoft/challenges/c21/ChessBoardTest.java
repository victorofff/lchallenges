package com.luxoft.challenges.c21;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.luxoft.challenges.c21.ChessBoard.Move;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ChessBoardTest {

    private ChessBoard chessBoard;

    @BeforeEach
    void init() {
        chessBoard = new ChessBoard();
    }


    @Test
    void testMoveListSingleMoves() {

        List<ChessBoard.Move> movesGenerated = chessBoard.getMoveList();

        List<ChessBoard.Move> cell0 = movesGenerated.stream().filter(move -> move.getP1() == 0).toList();
        assertEquals(1, cell0.size());
        assertEquals(0, cell0.get(0).getP1());
        assertEquals(4, cell0.get(0).getP2());
        assertEquals(9, cell0.get(0).getP3());

        List<ChessBoard.Move> cell3 = movesGenerated.stream().filter(move -> move.getP1() == 3).toList();
        assertEquals(1, cell3.size());
        assertEquals(3, cell3.get(0).getP1());
        assertEquals(6, cell3.get(0).getP2());
        assertEquals(10, cell3.get(0).getP3());

        List<ChessBoard.Move> cell31 = movesGenerated.stream().filter(move -> move.getP1() == 31).toList();
        assertEquals(1, cell31.size());
        assertEquals(31, cell31.get(0).getP1());
        assertEquals(27, cell31.get(0).getP2());
        assertEquals(22, cell31.get(0).getP3());

        List<ChessBoard.Move> cell28 = movesGenerated.stream().filter(move -> move.getP1() == 28).toList();
        assertEquals(1, cell28.size());
        assertEquals(28, cell28.get(0).getP1());
        assertEquals(25, cell28.get(0).getP2());
        assertEquals(21, cell28.get(0).getP3());

    }

    @Test
    void testMoveListValid() {

        List<ChessBoard.Move> movesGenerated = chessBoard.getMoveList();
        assertEquals(movesGenerated.stream().filter(move -> move.getP1() < 0).count(), 0);
        assertEquals(movesGenerated.stream().filter(move -> move.getP2() < 0).count(), 0);
        assertEquals(movesGenerated.stream().filter(move -> move.getP3() < 0).count(), 0);
    }

    @Test
    void testMoveListLeftMove() {

        List<ChessBoard.Move> movesGenerated = chessBoard.getMoveList();

        List<ChessBoard.Move> cell15 = movesGenerated.stream().filter(move -> move.getP1() == 15).toList();
        assertEquals(2, cell15.size());

        assertEquals(15, cell15.get(0).getP1());
        assertEquals(19, cell15.get(0).getP2());
        assertEquals(22, cell15.get(0).getP3());

        assertEquals(15, cell15.get(1).getP1());
        assertEquals(11, cell15.get(1).getP2());
        assertEquals(6, cell15.get(1).getP3());

    }

    @Test
    void testMoveListRightMove() {

        List<ChessBoard.Move> movesGenerated = chessBoard.getMoveList();

        List<ChessBoard.Move> cell16 = movesGenerated.stream().filter(move -> move.getP1() == 16).toList();
        assertEquals(2, cell16.size());

        assertEquals(16, cell16.get(0).getP1());
        assertEquals(12, cell16.get(0).getP2());
        assertEquals(9, cell16.get(0).getP3());

        assertEquals(16, cell16.get(1).getP1());
        assertEquals(20, cell16.get(1).getP2());
        assertEquals(25, cell16.get(1).getP3());

    }

    @Test
    void testMoveFirstStepsToTopVanilla() {

        int oldMoveSize = chessBoard.getMoveList().size();

        chessBoard.moveFirstStepsToTop(5, 1);
        assertEquals(oldMoveSize, chessBoard.getMoveList().size());

        List<ChessBoard.Move> cell13 = chessBoard.getMoveList().stream().filter(move -> move.getP1() == 13).toList();
        assertEquals(4, cell13.size());
        assertEquals(13, cell13.get(0).getP1());
        assertEquals(13, cell13.get(1).getP1());
        assertEquals(13, cell13.get(2).getP1());
        assertEquals(13, cell13.get(3).getP1());

        List<ChessBoard.Move> cell29 = chessBoard.getMoveList().stream().filter(move -> move.getP1() == 29).toList();
        assertEquals(2, cell29.size());

        assertEquals(29, cell29.get(0).getP1());
        assertEquals(29, cell29.get(1).getP1());

    }


    @Test
    void testPrintBoard() {

        chessBoard.populateBoardInitial(5, 1);

        String result = chessBoard.boardToString();
        assertFalse(result.isEmpty());

    }

    @Test
    void testPopulateBoardInitialVanilla() {
        chessBoard.populateBoardInitial(5, 1);
        assertEquals(chessBoard.getBoard()[20], 0x00);
    }

    @Test
    void testPopulateBoardInitialInvalidCell() {
        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> {
            chessBoard.populateBoardInitial(5, 2);
        });

    }


    @Test
    void testFastMoveArrayP1() {
        chessBoard.prepareFastMoveArrayP1();
        assertEquals(chessBoard.getMoveFastP1()[0][0].getP1(), chessBoard.getMoveList().get(0).getP1());
        assertEquals(chessBoard.getMoveFastP1()[0][0].getP2(), chessBoard.getMoveList().get(0).getP2());
        assertEquals(chessBoard.getMoveFastP1()[0][0].getP3(), chessBoard.getMoveList().get(0).getP3());
        assertEquals(chessBoard.getMoveFastP1().length, ChessBoard.BOARD_SIZE);
    }

    @Test
    void testFastMoveArrayP3() {
        chessBoard.prepareFastMoveArrayP3();

        List<Move> moves = chessBoard.getMoveList().stream().filter(move -> move.getP3() == 0).toList();
        assertEquals(chessBoard.getMoveFastP3()[0][0].getP1(), moves.get(0).getP1());
        assertEquals(chessBoard.getMoveFastP3()[0][0].getP2(), moves.get(0).getP2());
        assertEquals(chessBoard.getMoveFastP3()[0][0].getP3(), moves.get(0).getP3());
        assertEquals(chessBoard.getMoveFastP3().length, ChessBoard.BOARD_SIZE);
        assertEquals(chessBoard.getMoveFastP3()[0].length, 1);
    }


    @Test
    void testGetBoardIdVanilla() {
        chessBoard.populateBoardInitial(5, 1);
        int id1 = chessBoard.getBoardId();

        chessBoard.populateBoardInitial(2, 6);
        int id2 = chessBoard.getBoardId();
        assertEquals(id1, id2);
    }


}