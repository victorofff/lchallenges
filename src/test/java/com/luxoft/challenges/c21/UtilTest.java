package com.luxoft.challenges.c21;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class UtilTest {

    @Test
    void rowColumnIndexVanillaTest() {

        int[] result = Util.getRowColumnFromIndex(0);
        assertEquals(result[0], 0);
        assertEquals(result[1], 0);

    }

    @Test
    void rowColumnIndexLastColumnOddRowTest() {

        int[] result = Util.getRowColumnFromIndex(6);
        assertEquals(result[0], 1);
        assertEquals(result[1], 5);

    }

    @Test
    void rowColumnIndexLastColumnEvenRowTest() {

        int[] result = Util.getRowColumnFromIndex(9);
        assertEquals(result[0], 2);
        assertEquals(result[1], 2);

    }

    @Test
    void rowColumnIndexLastIndexTest() {

        int[] result = Util.getRowColumnFromIndex(31);
        assertEquals(result[0], 7);
        assertEquals(result[1], 7);

    }

    @Test
    void rowColumnIndexMidIndexTest() {

        int[] result = Util.getRowColumnFromIndex(17);
        assertEquals(result[0], 4);
        assertEquals(result[1], 2);

    }

    @Test
    void rowColumnIndex11IdxTest() {

        int[] result = Util.getRowColumnFromIndex(11);
        assertEquals(result[0], 2);
        assertEquals(result[1], 6);

    }

    @Test
    void testGetIndexFromRowColumnVanilla() {

        assertEquals(0, Util.getIndexFromRowColumn(0, 0));
        assertEquals(31, Util.getIndexFromRowColumn(7, 7));
        assertEquals(15, Util.getIndexFromRowColumn(3, 7));
        assertEquals(10, Util.getIndexFromRowColumn(2, 4));

    }

}