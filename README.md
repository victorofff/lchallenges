# Challenge #C21

By the materials of:
https://davdata.nl/math/solitaire.html
https://markusthill.github.io/solving-peg-solitaire/

Details:
1. I found only possible 1 flip instead of 8 for the traditional peg solitaire. However, it significantly helped to reduce the calculation time
see ```ChessBoard.getBoardId()```
2. Please see ```BoardEngineTest``` for different strategy types. Though there are no principle differences,
just played with how we iterate through the board or through the possible moves
3. Trove hashset gave about 10% performance gain
4. ```BoardEnginePseudoParallelDecorator``` - I tried to run with parallel calculations, trying to parallelize the possible solution 
subtrees from the first step. But it happened that there is only one subtree is valid, so no need in that sort of parallelization.
